#!/usr/bin/env python
# -*- coding: utf-8 -*-

import getpass
import os
import sys
from mastodon import Mastodon
from mastodon.Mastodon import MastodonMalformedEventError, MastodonNetworkError, MastodonReadTimeout, MastodonAPIError 
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )
        #sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def write_parameter( parameter, file_path ):
  print("Setting up host parameters...")
  print("\n")
  pleroma_hostname = input("Enter Pleroma hostname: ")
  pleroma_db = input("Pleroma db name: ")
  pleroma_db_user = input("pleroma db user: ")
  fediverse_db = input("fediverse db name: ")
  fediverse_db_user = input("fediverse db user: ")
  multiprocessing_processes = input("Multiprocessing processes: ")

  with open(file_path, "w") as text_file:
    print("pleroma_hostname: {}".format(pleroma_hostname), file=text_file)
    print("pleroma_db: {}".format(pleroma_db), file=text_file)
    print("pleroma_db_user: {}".format(pleroma_db_user), file=text_file)
    print("fediverse_db: {}".format(fediverse_db), file=text_file)
    print("fediverse_db_user: {}".format(fediverse_db_user), file=text_file)
    print("multiprocessing_processes: {}".format(multiprocessing_processes), file=text_file)

def create_table(db, db_user, table, sql):

  conn = None
  try:

    conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
    cur = conn.cursor()

    print("Creating table.. "+table)
    # Create the table in PostgreSQL database
    cur.execute(sql)

    conn.commit()
    print("Table "+table+" created!")
    print("\n")

  except (Exception, psycopg2.DatabaseError) as error:

    print(error)

  finally:

    if conn is not None:

      conn.close()

#############################################################################################

# Load configuration from config file
config_filepath = "config.txt"
pleroma_hostname = get_parameter("pleroma_hostname", config_filepath) # E.g., pleroma.site
pleroma_db = get_parameter("pleroma_db", config_filepath) # E.g., pleroma_prod
pleroma_db_user = get_parameter("pleroma_db_user", config_filepath) # E.g., pleroma
fediverse_db = get_parameter("fediverse_db", config_filepath) # E.g., fediverse
fediverse_db_user = get_parameter("fediverse_db_user", config_filepath) # E.g., pleroma

############################################################
# create database
############################################################

conn = None

try:

  conn = psycopg2.connect(dbname='postgres',
      user=fediverse_db_user, host='',
      password='')

  conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

  cur = conn.cursor()

  print("Creating database " + fediverse_db + ". Please wait...")

  cur.execute(sql.SQL("CREATE DATABASE {}").format(
          sql.Identifier(fediverse_db))
      )
  print("Database " + fediverse_db + " created!")

except (Exception, psycopg2.DatabaseError) as error:

  print(error)

finally:

  if conn is not None:

    conn.close()

#############################################################################################

try:

  conn = None
  conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

except (Exception, psycopg2.DatabaseError) as error:

  print(error)
  # Load configuration from config file
  os.remove("config.txt")
  print("Exiting. Run setup again with right parameters")
  sys.exit(0)

if conn is not None:

  print("\n")
  print("Host parameters saved to config.txt!")
  print("\n")

############################################################
# Create needed tables
############################################################

print("Creating table...")

########################################

db = fediverse_db
db_user = fediverse_db_user
table = "federation"
sql = "create table "+table+" (server varchar(30) PRIMARY KEY, users int, federated_servers int, posts int, updated_at timestamptz,"
sql += "software varchar(10), version varchar(40))"
create_table(db, db_user, table, sql)

#######################################

db = fediverse_db
db_user = fediverse_db_user
table = "federated_servers"
sql = "create table "+table+" (datetime timestamptz PRIMARY KEY, visited int, mastodon int, others int, no_response int, ssl_error int, timeout_error int, connection_error int, total_servers int,"
sql += "total_users int, pleroma int, mastodon_users int, pleroma_users int, gnusocial int, gnusocial_users int, zap int, zap_users int, plume int, plume_users int, hubzilla int, hubzilla_users int,"
sql += "misskey int, misskey_users int, prismo int, prismo_users int, osada int, osada_users int, groundpolis int, gpolis_users int, ganggo int, ganggo_users int, squs int, squs_users int, peertube int,"
sql += "peertube_users int, friendica int, friendica_users int, pixelfed int, pixelfed_users int"
sql += "writefreely int, writefreely_users int, ravenvale int, ravenvale_users int")
create_table(db, db_user, table, sql)

#######################################

db = fediverse_db
db_user = fediverse_db_user
table = "evo"
sql = "create table "+table+" (datetime timestamptz PRIMARY KEY, visited int, mastodon int, others int, no_response int, ssl_error int, timeout_error int, connection_error int, total_servers int,"
sql += "total_users int, pleroma int, mastodon_users int, pleroma_users int, gnusocial int, gnusocial_users int, zap int, zap_users int, plume int, plume_users int, hubzilla int, hubzilla_users int,"
sql += "misskey int, misskey_users int, prismo int, prismo_users int, osada int, osada_users int, groundpolis int, gpolis_users int, ganggo int, ganggo_users int, squs int, squs_users int, peertube int,"
sql += "peertube_users int, friendica int, friendica_users int, pixelfed int, pixelfed_users int)"
create_table(db, db_user, table, sql)

#######################################

db = fediverse_db
db_user = fediverse_db_user
table = "sslerror_servers"
sql = "create table "+table+" (server varchar(30) PRIMARY KEY, added_at timestamptz, updated_at timestamptz, days varchar(30))"
create_table(db, db_user, table, sql)

#######################################

db = fediverse_db
db_user = fediverse_db_user
table = "connerror_servers"
sql = "create table "+table+" (server varchar(30) PRIMARY KEY, added_at timestamptz, updated_at timestamptz, days varchar(30))"
create_table(db, db_user, table, sql)

#######################################

db = fediverse_db
db_user = fediverse_db_user
table = "timeouterror_servers"
sql = "create table "+table+" (server varchar(30) PRIMARY KEY, added_at timestamptz, updated_at timestamptz, days varchar(30))"
create_table(db, db_user, table, sql)

#######################################

db = fediverse_db
db_user = fediverse_db_user
table = "noresponse_servers"
sql = "create table "+table+" (server varchar(30) PRIMARY KEY, added_at timestamptz, updated_at timestamptz, days varchar(30))"
create_table(db, db_user, table, sql)

#######################################

db = fediverse_db
db_user = fediverse_db_user
table = "others_servers"
sql = "create table "+table+" (server varchar(30) PRIMARY KEY, added_at timestamptz, updated_at timestamptz, days varchar(30))"
create_table(db, db_user, table, sql)

#######################################################
# add column 'version' to federation table if not exist

conn = None
try:

    conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
    cur = conn.cursor()

    cur.execute('alter table federation add column if not exists version varchar(40)')

    conn.commit()

except (Exception, psycopg2.DatabaseError) as error:

    print(error)

finally:

    if conn is not None:

        conn.close()

###########################################################################################################################
# add columns 'writefreely', 'writefreely_users', 'ravenvale' and 'ravenvale_users' to federated_servers table if not exist

conn = None
try:

    conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
    cur = conn.cursor()

    cur.execute('alter table federated_servers add column if not exists writefreely int')
    cur.execute('alter table federated_servers add column if not exists writefreely_users int')
    cur.execute('alter table federated_servers add column if not exists ravenvale int')
    cur.execute('alter table federated_servers add column if not exists ravenvale_users int')

    conn.commit()

except (Exception, psycopg2.DatabaseError) as error:

    print(error)

finally:

    if conn is not None:

        conn.close()

#####################################
print("Done!")
print("Now you can run setup.py!")
print("\n")
