#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
start_time = time.time()
from six.moves import urllib
from datetime import datetime
import pytz
from subprocess import call
from mastodon import Mastodon
import threading
import os
import json
import signal
import sys
import os.path
import requests
import operator
import calendar
import psycopg2
from importlib import reload
import multiprocessing
from itertools import product

from decimal import *
getcontext().prec = 2

def is_json(myjson):
	try:
		json_object = json.loads(myjson)
	except ValueError as e:
		return False
	return True

def act_error(error_table, insert_query, federated_server, now):

	conn = None

	try:

		conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

		cur = conn.cursor()

		cur.execute(insert_query, (federated_server, now, now, ""))

		cur.execute("SELECT added_at FROM " + error_table + " where server=(%s)", (federated_server,))

		row = cur.fetchone()
		delta = now-row[0]

		cur.execute("UPDATE " + error_table + " SET updated_at=(%s), days=(%s) where server=(%s)", (now, delta, federated_server))

		conn.commit()

		cur.close()

	except (Exception, psycopg2.DatabaseError) as error:

		print(error)

	finally:

		if conn is not None:

			conn.close()

def get_platform_users(table, platform):

	conn = None

	try:

		conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

		cur = conn.cursor()

		cur.execute("select sum(users)from " + table + " where software=(%s)", (platform,))

		row = cur.fetchone()
		if row[0] != None:
			  platform_users = row[0]
		else:
			  platform_users = 0

		cur.close()
		return platform_users

	except (Exception, psycopg2.DatabaseError) as error:

		print(error)

	finally:

		if conn is not None:

                    conn.close()

def get_platform_servers(table, platform):

	conn = None

	try:

		conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

		cur = conn.cursor()

		cur.execute("select count(server) from " + table + " where software=(%s)", (platform,))

		row = cur.fetchone()
		if row != None:
			  platform_servers = row[0]
		else:
			  platform_servers = 0

		cur.close()
		return platform_servers

	except (Exception, psycopg2.DatabaseError) as error:

		print(error)

	finally:

		if conn is not None:

			conn.close()

def get_error_servers(table):

	global now
	now = str(now).replace("+01:00","")

	conn = None

	try:

		conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

		cur = conn.cursor()

		cur.execute("select count(server) from " + table + " where updated_at=(%s)", (now,))

		row = cur.fetchone()
		if row != None:
			error_servers = row[0]
		else:
			error_servers = 0

		cur.close()
		return error_servers

	except (Exception, psycopg2.DatabaseError) as error:

		print(error)

	finally:

		if conn is not None:

			conn.close()

def getserver(server):

  global sslerror
  global connectionerror
  global timeouterror
  global mastodont
  global pleroma
  global gnusocial
  global zap
  global plume
  global hubzilla
  global misskey
  global prismo
  global osada
  global groundpolis
  global ganggo
  global squs
  global peertube
  global friendica
  global pixelfed
  global writefreely
  global ravenvale
  global others
  global noresponse

  global pl_users
  global pl_users_total
  global mast_users
  global mast_users_total
  global gs_users
  global gs_users_total
  global zap_users
  global zap_users_total
  global plume_users
  global plume_users_total
  global hubzilla_users
  global hubzilla_users_total
  global misskey_users
  global misskey_users_total
  global prismo_users
  global prismo_users_total
  global osada_users
  global osada_users_total
  global gpolis_users
  global gpolis_users_total
  global ggg_users
  global ggg_users_total
  global squs_users
  global squs_users_total
  global peertube_users
  global peertube_users_total
  global friendica_users
  global friendica_users_total
  global pixelfed_users
  global pixelfed_users_total
  global writefreely_users
  global writefreely_users_total
  global ravenvale_users
  global ravenvale_users_total
  global total_users

  global total_servers
  global visited
  global server_soft
  global soft_version

  check_peertube = False
  check_zap = False
  check_plume = False
  check_hubzilla = False
  check_misskey = False
  check_prismo = False
  check_osada = False
  check_groundpolis = False
  check_ganggo = False
  check_squs = False
  check_gnusocial = False
  check_friendica = False
  check_pixelfed = False
  check_writefreely = False
  check_raven = False
  check_gnusocial2 = False

  users = 0
  instances = 0
  server_posts = 0
  server_soft = ""

  try:

    res = requests.get('https://' + server + '/api/v1/instance?',timeout=3)
    res_friendica = requests.get('https://' + server + '/nodeinfo/2.0?',timeout=3)
    check_pix = "compatible; Pixelfed" in res.text
    check_ravenvale = "ravenvale" in res.text
    check_friendica = "friendica" in res_friendica.text
    if check_friendica == False:
      check_friendica = "Friendica" in res_friendica.text

    if (res.ok) and check_pix == False and check_ravenvale == False and check_friendica == False:

      if res.json()['stats']['user_count'] != "":

        instances = res.json()['stats']['domain_count']

        nodeinfo = requests.get('https://' + server + '/nodeinfo/2.0.json?',timeout=3)
        check_pleroma = "pleroma" in nodeinfo.text.lower()

        if nodeinfo.status_code == 200 and check_pleroma == True:

          print("Pleroma Server")
          pl_users = nodeinfo.json()['usage']['users']['total']
          pl_users_total = pl_users_total + pl_users
          users = pl_users
          server_posts = res.json()['stats']['status_count']
          soft_version = nodeinfo.json()['software']['version']
          soft_version = (soft_version[:37] + '... ') if len(soft_version) > 37 else soft_version
          server_soft = "Pleroma"
          print(server, pl_users, instances, server_posts)
          print("\n")

        else:

          print("Mastodon Server")
          server_soft = "Mastodon"
          mast_users = res.json()['stats']['user_count']
          if mast_users != None:
            mast_users_total = mast_users_total + mast_users
          users = mast_users
          server_posts = res.json()['stats']['status_count']
          soft_version = res.json()['version']
          soft_version = (soft_version[:37] + '... ') if len(soft_version) > 37 else soft_version
          print(server, soft_version, mast_users, instances, server_posts)
          print("\n")

        insert_query = """INSERT INTO federation(server, users, federated_servers, posts, updated_at, software, version)
                               VALUES(%s,%s,%s,%s,%s,%s,%s) ON CONFLICT DO NOTHING"""
        conn = None

        try:

          conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

          cur = conn.cursor()

          cur.execute(insert_query, (server, users, instances, server_posts, now, server_soft, soft_version))

          cur.execute("UPDATE federation SET users=(%s), federated_servers=(%s), posts=(%s), updated_at=(%s), software=(%s), version=(%s) where server=(%s)",
                     (users, instances, server_posts, now, server_soft, soft_version, server))

          conn.commit()

          cur.close()

        except (Exception, psycopg2.DatabaseError) as error:
          print(error)

        finally:

          if conn is not None:
            conn.close()
          return

  except KeyError as e:

    print(server + "** Keyerror!: ")
    print("Keyerror " + str(e))
    print("\n")
    error_table = "others_servers"
    federated_server = server
    insert_query = "INSERT INTO " + error_table + "(server, added_at, updated_at, days) VALUES(%s,%s,%s,%s) ON CONFLICT DO NOTHING"

    act_error(error_table, insert_query, federated_server, now)
    return

  except ValueError as verr:

    print("Server " + server + " : " + str(verr))
    print("Others servers")
    print("\n")
    return

  except requests.exceptions.SSLError as errssl:

    print(server + " " + str(errssl))
    print("\n")
    error_table = "sslerror_servers"
    federated_server = server
    insert_query = "INSERT INTO " + error_table + "(server, added_at, updated_at, days) VALUES(%s,%s,%s,%s) ON CONFLICT DO NOTHING"

    act_error(error_table, insert_query, federated_server, now)
    return

  except requests.exceptions.HTTPError as errh:

    print("2 - Http Error:",errh)
    return

  except requests.exceptions.ConnectionError as errc:

    print(server + " ** connection error: ")
    print("\n")
    error_table = "connerror_servers"
    federated_server = server
    insert_query = "INSERT INTO " + error_table + "(server, added_at, updated_at, days) VALUES(%s,%s,%s, %s) ON CONFLICT DO NOTHING"

    act_error(error_table, insert_query, federated_server, now)
    return

  except requests.exceptions.Timeout as errt:

    print(server + " ** timeout error")
    print("\n")
    error_table = "timeouterror_servers"
    federated_server = server
    insert_query = "INSERT INTO " + error_table + "(server, added_at, updated_at, days) VALUES(%s,%s,%s,%s) ON CONFLICT DO NOTHING"

    act_error(error_table, insert_query, federated_server, now)
    return

  except requests.exceptions.RequestException as err:

    print("5 - OOps: Something Else",err)

  else:

    try:

      soft = ""
      others_nodeinfo = requests.get('https://' + server + '/nodeinfo/2.0?',timeout=3)
      gs_nodeinfo = requests.get('https://' + server + '/main/nodeinfo/2.0?',timeout=3)
      nodeinfo = requests.get('https://' + server + '/nodeinfo/2.0.json?',timeout=3)
      statusnet = requests.get('https://' + server + '/api/statusnet/config?',timeout=3)
      px_nodeinfo = requests.get('https://' + server + '/api/nodeinfo/2.0.json?',timeout=3)
      wf_nodeinfo = requests.get('https://' + server + '/api/nodeinfo?',timeout=3)

      try:

        if is_json(nodeinfo.text) == True:
          if nodeinfo.json() != None:
            soft = nodeinfo.json()['software']['name']
            if soft == "peertube":
              check_peertube = True
            elif soft == "prismo":
              check_prismo = True
            elif soft == "zap":
              check_zap = True
            elif soft == "osada":
              check_osada = True
            elif soft == "ravenvale":
              check_raven = True

        if is_json(others_nodeinfo.text) == True:
          soft = others_nodeinfo.json()['software']['name']
          if soft == "plume":
            check_plume = True
          elif soft == "hubzilla":
            check_hubzilla = True
          elif soft == "misskey":
            check_misskey = True
          elif soft == "groundpolis":
            check_groundpolis = True
          elif soft == "ganggo":
            check_ganggo = True
          elif soft == "squs":
            check_squs = True
          elif soft == "friendica" or soft == "Friendica":
            check_friendica = True

        if is_json(wf_nodeinfo.text) == True and soft == "":
          if wf_nodeinfo.json().get('error') != None and wf_nodeinfo.json().get('status') == None:
            soft = wf_nodeinfo.json()['software']['name']
            if soft == "writefreely":
              check_writefreely = True

        if is_json(gs_nodeinfo.text) == True:
          soft = gs_nodeinfo.json()['software']['name']
          if soft == "gnusocial":
            check_gnusocial = True

        if is_json(statusnet.text) == True and soft == "":
          if soft != "zap" and soft != "osada":
            soft = statusnet.json()['site']['friendica']['FRIENDICA_PLATFORM']
            if soft == "Friendica":
              check_friendica = True

        if is_json(px_nodeinfo.text) == True and soft == "":
           if soft != "Friendica" and soft != "hubzilla":
             soft = px_nodeinfo.json()['software']['name']
             if soft == "pixelfed":
               check_pixelfed = True
             if soft == "gnusocial":
               check_gnusocial2 = True

      except :

        print("JSONDecodeError")

      if check_peertube == False and check_zap == False and check_plume == False and check_hubzilla == False and check_misskey == False and check_prismo == False and check_osada == False and check_groundpolis == False and check_ganggo == False and check_squs == False and check_gnusocial == False and check_friendica == False and check_pixelfed == False and check_writefreely == False and check_raven == False and check_gnusocial2 == False:

        print(server + " ** no response")
        print("\n")
        error_table = "noresponse_servers"
        federated_server = server
        insert_query = "INSERT INTO " + error_table + "(server, added_at, updated_at, days) VALUES(%s,%s,%s,%s) ON CONFLICT DO NOTHING"

        act_error(error_table, insert_query, federated_server, now)

        api_respon = 0

      elif check_peertube == True or check_zap == True or check_plume == True or check_hubzilla == True or check_misskey == True or check_prismo == True or check_osada == True or check_groundpolis == True or check_ganggo == True or check_squs == True or check_gnusocial == True or check_friendica == True or check_pixelfed == True or check_writefreely == True or check_raven == True or check_gnusocial2 == True:

        api_respon = 1

        if nodeinfo.ok == True and check_peertube == True:

          print("Peertube Server")
          peertube_users = nodeinfo.json()['usage']['users']['total']
          peertube_users_total = peertube_users_total + peertube_users
          users = peertube_users
          instances = 0
          server_posts = nodeinfo.json()['usage']['localPosts']
          soft_version = nodeinfo.json()['software']['version']
          soft_version = (soft_version[:37] + '... ') if len(soft_version) > 37 else soft_version
          server_soft = "Peertube"
          print(server, peertube_users, instances, server_posts)
          print("\n")

        elif nodeinfo.ok == True and check_raven == True:

          print("Ravenvale Server")
          ravenvale_users = nodeinfo.json()['usage']['users']['total']
          ravenvale_users_total = ravenvale_users_total + ravenvale_users
          users = ravenvale_users
          instances = 0
          server_posts = nodeinfo.json()['usage']['localPosts']
          soft_version = nodeinfo.json()['software']['version']
          soft_version = (soft_version[:37] + '... ') if len(soft_version) > 37 else soft_version
          server_soft = "Ravenvale"
          print(server, ravenvale_users, instances, server_posts)
          print("\n")

        elif others_nodeinfo.ok == True and check_zap == True:

          print("Zap Server")
          zap_users = others_nodeinfo.json()['usage']['users']['total']
          zap_users_total = zap_users_total + zap_users
          users = zap_users
          server_posts = others_nodeinfo.json()['usage']['localPosts']
          instances = 0
          server_soft = "Zap"
          print(server, zap_users, instances, server_posts)
          print("\n")

        elif others_nodeinfo.ok == True and check_plume == True:

          print("Plume Server")
          plume_users = others_nodeinfo.json()['usage']['users']['total']
          plume_users_total = plume_users_total + plume_users
          users = plume_users
          server_posts = others_nodeinfo.json()['usage']['localPosts']
          soft_version = others_nodeinfo.json()['software']['version']
          soft_version = (soft_version[:37] + '... ') if len(soft_version) > 37 else soft_version
          instances = 0
          server_soft = "Plume"
          print(server, plume_users, instances, server_posts)
          print("\n")

        elif others_nodeinfo.ok == True and check_hubzilla == True:

          print("Hubzilla Server")
          hubzilla_users = others_nodeinfo.json()['usage']['users']['total']
          hubzilla_users_total = hubzilla_users_total + hubzilla_users
          users = hubzilla_users
          server_posts = others_nodeinfo.json()['usage']['localPosts']
          soft_version = others_nodeinfo.json()['software']['version']
          soft_version = (soft_version[:37] + '... ') if len(soft_version) > 37 else soft_version
          instances = 0
          server_soft = "Hubzilla"
          print(server, hubzilla_users, instances, server_posts)
          print("\n")

        elif others_nodeinfo.ok == True and check_misskey == True:

          print("Misskey Server")
          misskey_users = 0
          misskey_users_total = misskey_users_total + misskey_users
          users = misskey_users
          server_posts = 0
          instances = 0
          server_soft = "Misskey"
          print(server, misskey_users, instances, server_posts)
          print("\n")

        elif others_nodeinfo.ok == True and check_prismo == True:

          print("Prismo Server")
          prismo_users = others_nodeinfo.json()['usage']['users']['total']
          prismo_users_total = prismo_users_total + prismo_users
          users = prismo_users
          server_posts = others_nodeinfo.json()['usage']['localPosts']
          soft_version = others_nodeinfo.json()['software']['version']
          soft_version = (soft_version[:37] + '... ') if len(soft_version) > 37 else soft_version
          instances = 0
          server_soft = "Prismo"
          print(server, prismo_users, instances, server_posts)
          print("\n")

        elif others_nodeinfo.ok == True and check_osada == True:

          print("Osada Server")
          osada_users = others_nodeinfo.json()['usage']['users']['total']
          osada_users_total = osada_users_total + osada_users
          users = osada_users
          server_posts = others_nodeinfo.json()['usage']['localPosts']
          instances = 0
          server_soft = "Osada"
          print(server, osada_users, instances, server_posts)
          print("\n")

        elif others_nodeinfo.ok == True and check_groundpolis == True:

          print("Groundpolis Server")
          gpolis_users = others_nodeinfo.json()['usage']['users']['total']
          gpolis_users_total = gpolis_users_total + gpolis_users
          users = gpolis_users
          server_posts = others_nodeinfo.json()['usage']['localPosts']
          instances = 0
          server_soft = "Groundpolis"
          print(server, gpolis_users, instances, server_posts)
          print("\n")

        elif others_nodeinfo.ok == True and check_ganggo == True:

          print("Ganggo Server")
          ggg_users = others_nodeinfo.json()['usage']['users']['total']
          ggg_users_total = ggg_users_total + ggg_users
          users = ggg_users
          server_posts = others_nodeinfo.json()['usage']['localPosts']
          instances = 0
          server_soft = "Ganggo"
          print(server, ggg_users, instances, server_posts)
          print("\n")

        elif others_nodeinfo.ok == True and check_squs == True:

          print("Squs Server")
          squs_users = others_nodeinfo.json()['usage']['users']['total']
          squs_users_total = squs_users_total + squs_users
          users = squs_users
          server_posts = others_nodeinfo.json()['usage']['localPosts']
          instances = 0
          server_soft = "Squs"
          print(server, squs_users, instances, server_posts)
          print("\n")

        elif others_nodeinfo.ok == True and check_friendica == True:

          print("Friendica Server")
          if others_nodeinfo.json()['usage'] != []:
            friendica_users = others_nodeinfo.json()['usage']['users']['total']
            server_posts = others_nodeinfo.json()['usage']['localPosts']
          else:
            friendica_users = 0
            server_posts = 0
          friendica_users_total = friendica_users_total + friendica_users
          users = friendica_users
          instances = 0
          soft_version = others_nodeinfo.json()['software']['version']
          soft_version = (soft_version[:37] + '... ') if len(soft_version) > 37 else soft_version
          server_soft = "Friendica"
          print(server, friendica_users, instances, server_posts)
          print("\n")
          check_friendica = False

        elif gs_nodeinfo.ok == True and check_gnusocial == True:

          print("GNU Social Server")
          gs_users = gs_nodeinfo.json()['usage']['users']['total']
          if gs_users == 0:
            gs_users = gs_nodeinfo.json()['usage']['users']['activeHalfyear']
          gs_users_total = gs_users_total + gs_users
          users = gs_users
          server_posts = gs_nodeinfo.json()['usage']['localPosts']
          instances = 0
          server_soft = "GNU Social"
          soft_version = gs_nodeinfo.json()['software']['version']
          soft_version = (soft_version[:37] + '... ') if len(soft_version) > 37 else soft_version
          print(server, gs_users, instances, server_posts)
          print("\n")

        elif statusnet.ok == True and check_friendica == True: #statusnet.json()['site']['friendica']['FRIENDICA_PLATFORM'] == "Friendica":

          print("Friendica Server")
          friendica_users = 0
          friendica_users_total = friendica_users_total + friendica_users
          users = friendica_users
          instances = 0
          server_posts = 0
          soft_version = statusnet.json()['site']['friendica']['FRIENDICA_VERSION']
          soft_version = (soft_version[:37] + '... ') if len(soft_version) > 37 else soft_version
          server_soft = "Friendica"
          print(server, friendica_users, instances, server_posts)
          print("\n")

        elif px_nodeinfo.ok == True and check_pixelfed == True:

          print("Pixelfed Server")
          pixelfed_users = px_nodeinfo.json()['usage']['users']['total']
          pixelfed_users_total = pixelfed_users_total + pixelfed_users
          users = pixelfed_users
          instances = 0
          server_posts = px_nodeinfo.json()['usage']['localPosts']
          soft_version = px_nodeinfo.json()['software']['version']
          soft_version = (soft_version[:37] + '... ') if len(soft_version) > 37 else soft_version
          server_soft = "Pixelfed"
          print(server, pixelfed_users, instances, server_posts)
          print("\n")

        elif px_nodeinfo.ok == True and check_gnusocial2 == True:

          print("GNU Social 2.x Server")
          gs_users = px_nodeinfo.json()['usage']['users']['total']
          if gs_users == 0:
            gs_users = px_nodeinfo.json()['usage']['users']['activeHalfyear']
          gs_users_total = gs_users_total + gs_users
          users = gs_users
          instances = 0
          server_posts = px_nodeinfo.json()['usage']['localPosts']
          soft_version = px_nodeinfo.json()['software']['version']
          soft_version = (soft_version[:37] + '... ') if len(soft_version) > 37 else soft_version
          server_soft = "GNU Social"
          print(server, gs_users, instances, server_posts)
          print("\n")

        elif wf_nodeinfo.ok == True and check_writefreely == True:

          print("WriteFreely Server")
          writefreely_users = wf_nodeinfo.json()['usage']['users']['total']
          writefreely_users_total = writefreely_users_total + writefreely_users
          users = writefreely_users
          instances = 0
          server_posts = wf_nodeinfo.json()['usage']['localPosts']
          server_soft = "WriteFreely"
          print(server, writefreely_users, instances, server_posts)
          print("\n")

        else:

          print(server + " ** no response")

          error_table = "noresponse_servers"
          federated_server = server
          insert_query = "INSERT INTO " + error_table + "(server, added_at, updated_at, days) VALUES(%s,%s,%s,%s) ON CONFLICT DO NOTHING"

          act_error(error_table, insert_query, federated_server, now)

          api_respon = 0

      if api_respon == 1 and others_nodeinfo.ok or gs_nodeinfo.ok or nodeinfo.ok or statusnet.ok or px_nodeinfo.ok or wf_nodeinfo.ok:

        insert_query = """INSERT INTO federation(server, users, federated_servers, posts, updated_at, software, version)
                                                         VALUES(%s,%s,%s,%s,%s,%s,%s) ON CONFLICT DO NOTHING"""
        conn = None

        try:

          conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

          cur = conn.cursor()

          cur.execute(insert_query, (server, users, instances, server_posts, now, server_soft, soft_version))

          cur.execute("UPDATE federation SET users=(%s), federated_servers=(%s), posts=(%s), updated_at=(%s), software=(%s), version=(%s) where server=(%s)",
                       (users, instances, server_posts, now, server_soft, soft_version, server))

          conn.commit()

          cur.close()
          return

        except (Exception, psycopg2.DatabaseError) as error:

          print(error)
          return

        finally:

          if conn is not None:

            conn.close()
        return

    except KeyError as e:

      print(server + "** Keyerror!")
      print("Keyerror " + str(e))
      print("\n")
      error_table = "others_servers"
      federated_server = server
      insert_query = "INSERT INTO " + error_table + "(server, added_at, updated_at, days) VALUES(%s,%s,%s,%s) ON CONFLICT DO NOTHING"

      act_error(error_table, insert_query, federated_server, now)
      return

    except ValueError as verr:

      print("ValueError!")
      print(verr)
      print("Server (ValeuError): " + server)
      print("Other servers")
      print("\n")
      return

    except requests.exceptions.ConnectionError as errc:

      print(server + " ** connection error")
      print("\n")
      error_table = "connerror_servers"
      federated_server = server
      insert_query = "INSERT INTO " + error_table + "(server, added_at, updated_at, days) VALUES(%s,%s,%s, %s) ON CONFLICT DO NOTHING"

      act_error(error_table, insert_query, federated_server, now)
      return

      ########################################################################################

    except requests.exceptions.Timeout as errt:

      print(server + " ** timeout error")
      print("\n")
      error_table = "timeouterror_servers"
      federated_server = server
      insert_query = "INSERT INTO " + error_table + "(server, added_at, updated_at, days) VALUES(%s,%s,%s,%s) ON CONFLICT DO NOTHING"

      act_error(error_table, insert_query, federated_server, now)
      return

###############################################################################
# INITIALISATION
###############################################################################

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

# Load secrets from secrets file
secrets_filepath = "secrets/secrets.txt"
uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

# Load configuration from config file
config_filepath = "config.txt"
pleroma_hostname = get_parameter("pleroma_hostname", config_filepath) # E.g., pleroma.site
pleroma_db = get_parameter("pleroma_db", config_filepath) # E.g., pleroma_prod
pleroma_db_user = get_parameter("pleroma_db_user", config_filepath) # E.g., pleroma
fediverse_db = get_parameter("fediverse_db", config_filepath) # E.g., pleroma_federation
fediverse_db_user = get_parameter("fediverse_db_user", config_filepath) # E.g., pleroma
multiprocessing_processes = get_parameter("multiprocessing_processes", config_filepath) # E.g., cpu cores number)

# Initialise Mastodon API
mastodon = Mastodon(
    client_id = uc_client_id,
    client_secret = uc_client_secret,
    access_token = uc_access_token,
    api_base_url = 'https://' + pleroma_hostname,
)

# Initialise access headers
headers={ 'Authorization': 'Bearer %s'%uc_access_token }

###############################################################################
# get rejected instances from API to avoid getting information from them
###############################################################################

res = requests.get('https://' + pleroma_hostname + '/nodeinfo/2.0.json?')
rejected = res.json()['metadata']['federation']['mrf_simple']['reject']

###############################################################################
# federated servers with pleroma host - @spla@pleroma.cat - 6.9.2019
# SELECT distinct split_part(nickname, '@', 2) FROM users;
###########################################################################################

sslerror = 0
connectionerror = 0
timeouterror= 0
mastodont = 0
pleroma = 0
gnusocial = 0
zap = 0
plume = 0
hubzilla = 0
misskey = 0
prismo = 0
osada = 0
groundpolis = 0
ganggo = 0
squs = 0
peertube = 0
friendica = 0
pixelfed = 0
others = 0
noresponse = 0

pl_users = 0
pl_users_total = 0
mast_users = 0
mast_users_total = 0
gs_users = 0
gs_users_total = 0
zap_users = 0
zap_users_total = 0
plume_users = 0
plume_users_total = 0
hubzilla_users = 0
hubzilla_users_total = 0
misskey_users = 0
misskey_users_total = 0
prismo_users = 0
prismo_users_total = 0
osada_users = 0
osada_users_total = 0
gpolis_users = 0
gpolis_users_total = 0
ggg_users = 0
ggg_users_total = 0
squs_users = 0
squs_users_total = 0
peertube_users = 0
peertube_users_total = 0
friendica_users = 0
friendica_users_total = 0
pixelfed_users = 0
pixelfed_users_total = 0
total_users = 0

total_servers = 0
visited = 0
server_soft = ""

tz = pytz.timezone('Europe/Madrid')
now = datetime.now(tz)

try:

	conn = None
	conn = psycopg2.connect(database = pleroma_db, user = pleroma_db_user, password = "", host = "/var/run/postgresql", port = "5432")

	cur = conn.cursor()

	### get federated servers from Pleroma database

	cur.execute("SELECT distinct split_part(nickname, '@', 2) FROM users")

	federated_servers = []

	for row in cur:

		if row[0] != None:

			federated_servers.append(row[0]) ## save them to federated_servers array

	cur.close()

except (Exception, psycopg2.DatabaseError) as error:

	print(error)

finally:

	if conn is not None:

		conn.close()

###########################################################################
# remove rejected servers from federated_servers array because
# we don't want to collect their stats
###########################################################################

i = 0

while i < len(rejected):

	if rejected[i] in federated_servers:
		federated_servers.remove(rejected[i])

	i += 1

###########################################################################
# do multiprocessing

processes_number = int(multiprocessing_processes)
with multiprocessing.Pool(processes=processes_number) as pool:
	results = pool.starmap(getserver, product(federated_servers))
print(results)

###############################################################################################
# get servers with error

table ='others_servers'
others = get_error_servers(table)

table ='connerror_servers'
connectionerror = get_error_servers(table)

table ='noresponse_servers'
noresponse = get_error_servers(table)

table ='sslerror_servers'
sslerror = get_error_servers(table)

table ='timeouterror_servers'
timeouterror = get_error_servers(table)

################################################################################################
# How many servers of each software
################################################################################################

table = 'federation'
platform = 'Mastodon'
mastodont = get_platform_servers(table, platform)

platform = 'Pleroma'
pleroma = get_platform_servers(table, platform)

platform = 'Peertube'
peertube = get_platform_servers(table, platform)

platform = 'Friendica'
friendica = get_platform_servers(table, platform)

platform = 'Pixelfed'
pixelfed = get_platform_servers(table, platform)

platform = 'Misskey'
misskey = get_platform_servers(table, platform)

platform = 'Hubzilla'
hubzilla = get_platform_servers(table, platform)

platform = 'GNU Social'
gnusocial = get_platform_servers(table, platform)

platform = 'Plume'
plume = get_platform_servers(table, platform)

platform = 'Zap'
zap = get_platform_servers(table, platform)

platform = 'Osada'
osada = get_platform_servers(table, platform)

platform = 'Prismo'
prismo = get_platform_servers(table, platform)

platform = 'Groundpolis'
groundpolis = get_platform_servers(table, platform)

platform = 'Ravenvale'
ravenvale = get_platform_servers(table, platform)

platform = 'Squs'
squs = get_platform_servers(table, platform)

platform = 'Writefreely'
writefreely = get_platform_servers(table, platform)

platform = 'Ganggo'
ganggo = get_platform_servers(table, platform)

platform = 'Others'
others = get_platform_servers(table, platform)

total_servers = mastodont + pleroma + gnusocial + zap + plume + hubzilla + misskey + prismo + osada + groundpolis + ganggo + squs + peertube + friendica + pixelfed + writefreely + ravenvale + others
visited = len(federated_servers)

##################################################################################################
# get users of each software and total users

table = 'federation'
platform = 'Mastodon'
mast_users_total = get_platform_users(table, platform)

platform = 'Pleroma'
pl_users_total = get_platform_users(table, platform)

platform = 'Peertube'
peertube_users_total = get_platform_users(table, platform)

platform = 'Friendica'
friendica_users_total = get_platform_users(table, platform)

platform = 'Pixelfed'
pixelfed_users_total = get_platform_users(table, platform)

platform = 'Misskey'
misskey_users_total = get_platform_users(table, platform)

platform = 'Hubzilla'
hubzilla_users_total = get_platform_users(table, platform)

platform = 'GNU Social'
gs_users_total = get_platform_users(table, platform)

platform = 'Plume'
plume_users_total = get_platform_users(table, platform)

platform = 'Zap'
zap_users_total = get_platform_users(table, platform)

platform = 'Osada'
osada_users_total = get_platform_users(table, platform)

platform = 'Prismo'
prismo_users_total = get_platform_users(table, platform)

platform = 'Groundpolis'
gpolis_users_total = get_platform_users(table, platform)

platform = 'Ravenvale'
ravenvale_users_total = get_platform_users(table, platform)

platform = 'Squs'
squs_users_total = get_platform_users(table, platform)

platform = 'Writefreely'
writefreely_users_total = get_platform_users(table, platform)

platform = 'Ganggo'
ggg_users_total = get_platform_users(table, platform)

total_users = mast_users_total + pl_users_total + gs_users_total + zap_users_total + plume_users_total + hubzilla_users_total + misskey_users_total + prismo_users_total
total_users = total_users + osada_users_total + gpolis_users_total + ggg_users_total + squs_users_total + peertube_users_total + friendica_users_total + pixelfed_users_total
total_users = total_users + writefreely_users_total + ravenvale_users_total

###############################################################################################
# delete from database back on life servers                                                   #
###############################################################################################

conn = None

try:

	conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

	cur = conn.cursor()

	cur.execute("DELETE from sslerror_servers where updated_at < %s OR updated_at is null", (now,))

	cur.execute("DELETE from connerror_servers where updated_at < %s OR updated_at is null", (now,))

	cur.execute("DELETE from timeouterror_servers where updated_at < %s OR updated_at is null", (now,))

	conn.commit()

	cur.close()

except (Exception, psycopg2.DatabaseError) as error:

	print(error)

finally:

	if conn is not None:

	  conn.close()

########################### delete inactive servers from federation table

conn = None

try:

	conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

	cur = conn.cursor()

	cur.execute("DELETE from federation where updated_at < %s OR updated_at is null", (now,))

	conn.commit()

	cur.close()

except (Exception, psycopg2.DatabaseError) as error:

	print(error)

finally:

	if conn is not None:

	  conn.close()

################################################################################################
# get last check values
################################################################################################

select = """select visited, mastodon, others, no_response, ssl_error, timeout_error, connection_error, total_servers, total_users, pleroma, mastodon_users,
			   pleroma_users, gnusocial, gnusocial_users, zap, zap_users, plume, plume_users, hubzilla, hubzilla_users, misskey, misskey_users, prismo, prismo_users, osada, osada_users,
			   groundpolis, gpolis_users, ganggo, ganggo_users, squs, squs_users, peertube, peertube_users, friendica, friendica_users, pixelfed, pixelfed_users, writefreely, writefreely_users,
                           ravenvale, ravenvale_users from federated_servers order by datetime desc limit 1;"""
try:

    conn = None
    conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    cur.execute(select)

    row = cur.fetchone()

    visited_before = row[0]
    mastodont_before = row[1]
    others_before = row[2]
    noresponse_before = row[3]
    sslerror_before = row[4]
    timeout_error_before = row[5]
    connerror_before = row[6]
    serv_total_before = row[7]
    total_users_before = row[8]
    pleroma_before = row[9]
    mastodon_users_before = row[10]
    pleroma_users_before = row[11]
    gnusocial_before = row[12]
    gnusocial_users_before = row[13]
    zap_before = row[14]
    zap_users_before = row[15]
    plume_before = row[16]
    plume_users_before = row[17]
    hubzilla_before = row[18]
    hubzilla_users_before = row[19]
    misskey_before = row[20]
    misskey_users_before = row[21]
    prismo_before = row[22]
    prismo_users_before = row[23]
    osada_before = row[24]
    osada_users_before = row[25]
    groundpolis_before = row[26]
    groundpolis_users_before = row[27]
    ganggo_before = row[28]
    ganggo_users_before = row[29]
    squs_before = row[30]
    squs_users_before = row[31]
    peertube_before = row[32]
    peertube_users_before = row[33]
    friendica_before = row[34]
    friendica_users_before = row[35]
    pixelfed_before = row[36]
    pixelfed_users_before = row[37]
    writefreely_before = row[38]
    writefreely_users_before = row[39]
    ravenvale_before = row[40]
    ravenvale_users_before = row[41]

    cur.close()

    evo_visited = visited - visited_before
    evo_mastodont = mastodont - mastodont_before
    evo_others = others - others_before
    evo_noresponse = noresponse - noresponse_before
    evo_sslerror = sslerror - sslerror_before
    evo_timeout_error = timeouterror - timeout_error_before
    evo_connerror = connectionerror - connerror_before
    evo_serv_total = total_servers - serv_total_before
    evo_total_users = total_users - total_users_before
    evo_pleroma = pleroma - pleroma_before
    evo_mast_users = mast_users_total - mastodon_users_before
    evo_pl_users = pl_users_total - pleroma_users_before
    evo_gnusocial = gnusocial - gnusocial_before
    evo_gs_users = gs_users_total - gnusocial_users_before
    evo_zap = zap - zap_before
    evo_zap_users = zap_users_total - zap_users_before
    evo_plume = plume - plume_before
    evo_plume_users = plume_users_total - plume_users_before
    evo_hubzilla = hubzilla - hubzilla_before
    evo_hub_users = hubzilla_users_total - hubzilla_users_before
    evo_misskey = misskey - misskey_before
    evo_misskey_users = misskey_users_total - misskey_users_before
    evo_prismo = prismo - prismo_before
    evo_prismo_users = prismo_users_total - prismo_users_before
    evo_osada = osada - osada_before
    evo_osada_users = osada_users_total - osada_users_before
    evo_groundpolis = groundpolis - groundpolis_before
    evo_groundpolis_users = gpolis_users_total - groundpolis_users_before
    evo_ganggo = ganggo - ganggo_before
    evo_ganggo_users = ggg_users_total - ganggo_users_before
    evo_squs = squs - squs_before
    evo_squs_users = squs_users_total - squs_users_before
    evo_peertube = peertube - peertube_before
    evo_peertube_users = peertube_users_total - peertube_users_before
    evo_friendica = friendica - friendica_before
    evo_friendica_users = friendica_users_total - friendica_users_before
    evo_pixelfed = pixelfed - pixelfed_before
    evo_pixelfed_users = pixelfed_users_total - pixelfed_users_before
    evo_writefreely = writefreely - writefreely_before
    evo_writefreely_users = writefreely_users_total - writefreely_users_before
    evo_ravenvale = ravenvale - ravenvale_before
    evo_ravenvale_users = ravenvale_users_total - ravenvale_users_before

except (Exception, psycopg2.DatabaseError) as error:

    print(error)

finally:

    if conn is not None:

        conn.close()

################################################################################
#  save evolution to evo table
################################################################################

insert_line = """INSERT INTO evo(datetime, visited, mastodon, others, no_response, ssl_error, timeout_error, connection_error, total_servers, total_users, pleroma, mastodon_users,
			   pleroma_users, gnusocial, gnusocial_users, zap, zap_users, plume, plume_users, hubzilla, hubzilla_users, misskey, misskey_users, prismo, prismo_users, osada, osada_users,
			   groundpolis, gpolis_users, ganggo, ganggo_users, squs, squs_users, peertube, peertube_users, friendica, friendica_users, pixelfed, pixelfed_users) 
			   VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) RETURNING datetime;"""

conn = None

try:

	conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

	cur = conn.cursor()

	cur.execute(insert_line, (now, evo_visited, evo_mastodont, evo_others, evo_noresponse, evo_sslerror, evo_timeout_error, evo_connerror, evo_serv_total, evo_total_users, evo_pleroma, evo_mast_users,
								evo_pl_users, evo_gnusocial, evo_gs_users, evo_zap, evo_zap_users, evo_plume, evo_plume_users, evo_hubzilla, evo_hub_users, evo_misskey, evo_misskey_users, 
								evo_prismo, evo_prismo_users, evo_osada, evo_osada_users, evo_groundpolis, evo_groundpolis_users, evo_ganggo, evo_ganggo_users, evo_squs, evo_squs_users, 
								evo_peertube, evo_peertube_users, evo_friendica, evo_friendica_users, evo_pixelfed, evo_pixelfed_users))

	conn.commit()

	cur.close()

except (Exception, psycopg2.DatabaseError) as error:

	print(error)

finally:

	if conn is not None:

	  conn.close()

################################################################################

insert_line = """INSERT INTO federated_servers(datetime, visited, mastodon, others, no_response, ssl_error, timeout_error, connection_error, total_servers, total_users, pleroma, mastodon_users,
			   pleroma_users, gnusocial, gnusocial_users, zap, zap_users, plume, plume_users, hubzilla, hubzilla_users, misskey, misskey_users, prismo, prismo_users, osada, osada_users,
			   groundpolis, gpolis_users, ganggo, ganggo_users, squs, squs_users, peertube, peertube_users, friendica, friendica_users, pixelfed, pixelfed_users) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
			   %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) RETURNING datetime;"""
conn = None

try:

	conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

	cur = conn.cursor()

	cur.execute(insert_line, (now, visited, mastodont, others, noresponse, sslerror, timeouterror, connectionerror, total_servers, total_users, pleroma, mast_users_total, pl_users_total,
						  gnusocial, gs_users_total, zap, zap_users_total, plume, plume_users_total, hubzilla, hubzilla_users_total, misskey, misskey_users_total, prismo, prismo_users_total, 
						  osada, osada_users_total, groundpolis, gpolis_users_total, ganggo, ggg_users_total, squs, squs_users_total, peertube, peertube_users_total, friendica,
						  friendica_users_total, pixelfed, pixelfed_users_total))

	conn.commit()

	cur.close()

except (Exception, psycopg2.DatabaseError) as error:

	print(error)

finally:

	if conn is not None:

	  conn.close()

###############################################################################
# calc percentages
###############################################################################

print("Percentatges:")
print("Visited Domains : " + str(visited))

perc_no_response = round(((noresponse * 100.00) / visited), 2)
print("No response domains: " + str(noresponse) + " (" + str(perc_no_response) + "%)")

perc_error_ssl = round(((sslerror * 100.00) / visited), 2)
print("SSL error: " + str(sslerror) + " (" + str(perc_error_ssl) + "%)")

perc_error_tout = round(((timeouterror * 100.00) / visited), 2)
print("TimeOut error: " + str(timeouterror) + " (" + str(perc_error_tout) + "%)")

perc_error_conn = round(((connectionerror * 100.00) / visited), 2)
print("Connection error: " + str(connectionerror) + " (" + str(perc_error_conn) + "%)")

print("The fediverse:" + "\n")
print("Active servers: " + str(total_servers))
print("Registered users: " + str(total_users))

###############################################################################
# T  O  O  T !
###############################################################################

toot_text = "#fediverse view from " + pleroma_hostname + " \n"
toot_text += "\n"
toot_text += "Domains:" + "\n"
toot_text += "Visited " + str(visited) + " \n"
toot_text += "No response " + str(noresponse)  + " (" + str(perc_no_response) + "%)" + "\n"
toot_text += "SSL error " + str(sslerror)  + " (" + str(perc_error_ssl) + "%)" + "\n"
toot_text += "Timeout error " + str(timeouterror)  + " (" + str(perc_error_tout) + "%)" + "\n"
toot_text += "Connection error " + str(connectionerror)  + " (" + str(perc_error_conn) + "%)" + "\n"
toot_text += "\n"
toot_text += "The fediverse:" + "\n"
toot_text += "Total active servers: " + str(total_servers) + "\n"
toot_text += "Mastodon -> " + str(mastodont) + "\n"
toot_text += "Pleroma -> " + str(pleroma) + "\n"
toot_text += "Peertube -> " + str(peertube) + "\n"
toot_text += "Misskey -> " + str(misskey) + "\n"
toot_text += "Friendica -> " + str(friendica) + "\n"
toot_text += "Hubzilla -> " + str(hubzilla) + "\n"
toot_text += "Plume -> " + str(plume) + "\n"
toot_text += "GNU Social -> " + str(gnusocial) + "\n"
toot_text += "Pixelfed -> " + str(pixelfed) + "\n"
toot_text += "Zap -> " + str(zap) + "\n"
toot_text += "Groundpolis -> " + str(groundpolis) + "\n"
toot_text += "Prismo -> " + str(prismo) + "\n"
toot_text += "Osada -> " + str(osada) + "\n"
toot_text += "Ganggo -> " + str(ganggo) + "\n"
toot_text += "Squs -> " + str(squs) + "\n"
toot_text += "Writefreely -> " + str(writefreely) + "\n"
toot_text += "Ravenvale -> " + str(ravenvale) + "\n"
toot_text += "\n"
toot_text += "Total registered users: " + str(total_users) + "\n"
toot_text += "Mastodon -> " + str(mast_users_total) + "\n"
toot_text += "Pleroma: -> " + str(pl_users_total) + "\n"
toot_text += "Peertube -> " + str(peertube_users_total) + "\n"
toot_text += "Hubzilla -> " + str(hubzilla_users_total) + "\n"
toot_text += "GNU Social -> " + str(gs_users_total) + "\n"
toot_text += "Pixelfed -> " + str(pixelfed_users_total) + "\n"
toot_text += "Prismo -> " + str(prismo_users_total) + "\n"
toot_text += "Zap -> " + str(zap_users_total) + "\n"
toot_text += "Plume -> " + str(plume_users_total) + "\n"
toot_text += "Friendica -> " + str(friendica_users_total) + "\n"
toot_text += "Misskey -> " + str(misskey_users_total) + "\n"
toot_text += "Osada -> " + str(osada_users_total) + "\n"
toot_text += "GroundPolis -> " + str(gpolis_users_total) + "\n"
toot_text += "Ganggo -> " + str(ggg_users_total) + "\n"
toot_text += "Squs -> " + str(squs_users_total) + "\n"
toot_text += "Writefreely -> " + str(writefreely_users_total) + "\n"
toot_text += "Ravenvale -> " + str(ravenvale_users_total) + "\n"
toot_text += "\n"
toot_text += "Execution time: %s segons" % (time.time() - start_time)

print("Tooting...")
print(toot_text)

mastodon.status_post(toot_text, in_reply_to_id=None, )

print("Tooted succesfully!")

