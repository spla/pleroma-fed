# pleroma-fed
Your Pleroma server's known fediverse

You know how many servers are federated with your Pleroma server but you don't know much about them. What software are they running? How many users? Are they still alive?
These Python scripts will help you to get all that information and much more! 

### Dependencies

-   **Python 3**
-   Postgresql server
-   Everything else at the top of `pleroma-fed.py`!

### Usage:

Within Python Virtual Environment:

1. Run 'db-setup.py' to create needed database and tables. All collected data will be written there. You can use software like Grafana to visualize your fediverse metrics!

2. Run 'setup.py' to get your Pleroma's access token of an existing user. It will be saved to 'secrets/secrets.txt' for further use.

3. Run 'pleroma-fed.py' to start collecting all information about your Pleroma server's federated servers, their stats, if they are alive etc. 

Once it finished, will post some of collected stats to your Pleroma server!

Note: install all needed packages with 'pip install package' or use 'pip install -r requirements.txt' to install them. 

### 11.2.2020 New release with multiprocessing support!
    
    - db-setup.py will ask you how many processes you want pleroma-fed.py will use.
    - db-setup.py will add new table and columns to your fediverse database


